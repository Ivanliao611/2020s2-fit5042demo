package fit5042.tutex;

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;

/**
 *
 * TODO Exercise 1.3 Step 3 Complete this class. Please refer to tutorial instructions.
 * This is the main driver class. This class contains the main method for Exercise 1A
 * 
 * This program can run without the completion of Exercise 1B.
 * 
 * @author Junyang
 */
public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;
	private ArrayList<Property> list = new ArrayList<Property>();
    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
        
    }
    
    // this method is for initializing the property objects
    // complete this method
    public void createProperty() {
    	Property p1 = new Property();
    	p1.setId(1);
    	p1.setAddress("24 Boston Ave, Malvern East VIC 3145, Australia");
    	p1.setNumberOfBedrooms(2);
    	p1.setSize(150);
    	p1.setPrice(420000.00);
    	list.add(p1);
    	Property p2 = new Property();
    	p2.setId(2);
    	p2.setAddress("11 Bettina St, Clayton VIC 3168, Australia");
    	p2.setNumberOfBedrooms(3);
    	p2.setSize(352);
    	p2.setPrice(360000.00);
    	list.add(p2);
    	Property p3 = new Property();
    	p3.setId(3);
    	p3.setAddress("3 Wattle Ave, Glen Huntly VIC 3163, Australia");
    	p3.setNumberOfBedrooms(5);
    	p3.setSize(800);
    	p3.setPrice(650000.00);
    	list.add(p3);
    	Property p4 = new Property();
    	p4.setId(4);
    	p4.setAddress("3 Hamilton St, Bentleigh VIC 3204, Australia");
    	p4.setNumberOfBedrooms(2);
    	p4.setSize(170);
    	p4.setPrice(435000.00);
    	list.add(p4);
    	Property p5 = new Property();
    	p5.setId(5);
    	p5.setAddress("82 Spring Rd, Hapton East VIC 3188, Australia");
    	p5.setNumberOfBedrooms(1);
    	p5.setSize(60);
    	p5.setPrice(820000.00);
    	list.add(p5);
        
    }
    
    // this method is for displaying all the properties
    // complete this method
    public void displayProperties() {
    	for (int i = 0; i < 5; i++) {
    		System.out.println(list.get(i).getId() + " " + list.get(i).getAddress() + " " + list.get(i).getNumberOfBedrooms()+".0sqm $" + list.get(i).getSize());
    	}
    		
      
    }
    
    // this method is for searching the property by ID
    // complete this method
    public void searchPropertyById() {
        System.out.println("Enter the ID of the Property you want to search: ");
        Scanner input = new Scanner(System.in);
        int number = input.nextInt();
        System.out.println(list.get(number).getId() + " " + list.get(number).getAddress() + " " + list.get(number).getNumberOfBedrooms()+".0sqm $" + list.get(number).getSize());
    }
    
    public void run() {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
